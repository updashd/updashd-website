<?php
$projectUrl = "https://github.com/updashd";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="description" content="Extensible Server Monitoring" />
    <meta name="author" content="Updashd" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="icon" href="img/updashd-icon.ico" />
    <link href='https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css' />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/home.css" rel="stylesheet" />
    <title>Updashd</title>
</head>
<body>
<header>
    <div class="header-container">
        <div class="container">
            <div class="logo-container">
                <div class="logo-icon">
                    <img src="img/logo-icon-large.svg" alt="Updashd Logo" />
                </div>
                <div class="logo-name">
                    <h2>Updashd</h2>
                    <p>Extensible Server Monitoring</p>
                </div>
            </div>
        </div>
    </div>
</header>
<main>
    <div class="container body-container">
        <h2>Coming soon...</h2>
        <p>A scalable, flexible, and extensible network service monitoring solution.</p>

        <p>Learn more and contribute on
            <a href="<?php echo $projectUrl; ?>"
               onclick="ga('send', 'event', 'GitHub', 'Learn More', 'Learn more and contribute')">GitHub</a>
        </p>

        <img src="img/architecture-simple.png" />

        <p>Updashd is a new open source project. More contribution details will be available at a later time.</p>
    </div>
</main>
<footer>
    <div class="footer-container">
        <div class="container">
            <p>Open Source Software</p>
            <ul>
                <li>
                    <a href="<?php echo $projectUrl; ?>"
                       onclick="ga('send', 'event', 'GitHub', 'Footer Link')">GitHub</a>
                </li>
                <li>
                    <a href="http://docs.updashd.com/"
                       onclick="ga('send', 'event', 'Documentation', 'Footer Link')">Documentation</a>
                </li>
            </ul>
        </div>
    </div>
</footer>

<a href="<?php echo $projectUrl; ?>" onclick="ga('send', 'event', 'GitHub', 'Fork Me', 'Fork me on GitHub')">
    <img style="position: absolute; top: 0; right: 0; border: 0;"
         src="https://camo.githubusercontent.com/a6677b08c955af8400f44c6298f40e7d19cc5b2d/68747470733a2f2f73332e616d617a6f6e6177732e636f6d2f6769746875622f726962626f6e732f666f726b6d655f72696768745f677261795f3664366436642e706e67"
         alt="Fork me on GitHub"
         data-canonical-src="https://s3.amazonaws.com/github/ribbons/forkme_right_gray_6d6d6d.png">
</a>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-80212194-1', 'auto');
    ga('send', 'pageview');
</script>
</body>
</html>